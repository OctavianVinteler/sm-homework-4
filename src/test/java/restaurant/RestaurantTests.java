package restaurant;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class RestaurantTests {
	
	PrintStream out = null;
	
	@Before
	public void setUp()
	{
		out = Mockito.mock(PrintStream.class);
	    System.setOut(out);
	}
	
	@Test
	public void testStartGame()
	{
		Restaurant restaurant = new Restaurant("UT Restaurant (UTR)");
		Player player = new Player("Naved");
		restaurant.setBudget(10000);
		RestaurantGame game = new RestaurantGame();
	    game.setRestaurant(restaurant);
	    game.setPlayer(player);
	    game.startGame();
	    
	    Mockito.verify(out).println("Welcome to UT Restaurant (UTR) Game!");
	    Mockito.verify(out).println("Thank you, Naved.");
	    Mockito.verify(out).println("The starting budget equals to 10000");
	}
	
	@Test
	public void testCreateMenu()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    List<Dish> dishes = new ArrayList<Dish>();
	    List<Beverage> beverages = new ArrayList<Beverage>();
	    
	    Dish dish1 = new Dish();
	    dish1.setDishName("grilled chicken");
	    dish1.setCalories(400);
	    dish1.setQuality("high");
	    dish1.setCost(12);
	    dishes.add(dish1);
	    
	    Dish dish2 = new Dish();
	    dish2.setDishName("lasagna");
	    dish2.setCalories(800);
	    dish2.setQuality("low");
	    dish2.setCost(7);
	    dishes.add(dish2);
	    
	    Dish dish3 = new Dish();
	    dish3.setDishName("gnocchi");
	    dish3.setCalories(700);
	    dish3.setQuality("high");
	    dish3.setCost(12);
	    dishes.add(dish3);
	    
	    Dish dish4 = new Dish();
	    dish4.setDishName("pizza");
	    dish4.setCalories(400);
	    dish4.setQuality("low");
	    dish4.setCost(7);
	    dishes.add(dish4);
	    
	    Dish dish5 = new Dish();
	    dish5.setDishName("snitzel");
	    dish5.setCalories(400);
	    dish5.setQuality("high");
	    dish5.setCost(12);
	    dishes.add(dish5);
	    
	    Beverage beverage1 = new Beverage();
	    beverage1.setBeverageName("coke");
	    beverage1.setVolume(35);
	    beverage1.setQuality("high");
	    beverage1.setCost(5);
	    beverages.add(beverage1);
		
	    Beverage beverage2 = new Beverage();
	    beverage2.setBeverageName("fanta");
	    beverage2.setVolume(35);
	    beverage2.setQuality("low");
	    beverage2.setCost(2);
	    beverages.add(beverage2);
	    
	    Beverage beverage3 = new Beverage();
	    beverage3.setBeverageName("wine");
	    beverage3.setVolume(50);
	    beverage3.setQuality("high");
	    beverage3.setCost(5);
	    beverages.add(beverage3);
	    
	    Beverage beverage4 = new Beverage();
	    beverage4.setBeverageName("beer");
	    beverage4.setVolume(50);
	    beverage4.setQuality("low");
	    beverage4.setCost(2);
	    beverages.add(beverage4);
	    
	    Beverage beverage5 = new Beverage();
	    beverage5.setBeverageName("sprite");
	    beverage5.setVolume(35);
	    beverage5.setQuality("high");
	    beverage5.setCost(5);
	    beverages.add(beverage5);
	    
	    restaurant.createMenu(dishes, beverages);
	    Mockito.verify(out).println("Restaurant menu is created");
	}
	
	@Test
	public void testIncorrectHighDishesCost()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    List<Dish> dishes = new ArrayList<Dish>();
	    List<Beverage> beverages = new ArrayList<Beverage>();
	    
	    Dish dish1 = new Dish();
	    dish1.setDishName("grilled chicken");
	    dish1.setCalories(400);
	    dish1.setQuality("high");
	    dish1.setCost(12);
	    dishes.add(dish1);
	    
	    Dish dish2 = new Dish();
	    dish2.setDishName("lasagna");
	    dish2.setCalories(800);
	    dish2.setQuality("low");
	    dish2.setCost(7);
	    dishes.add(dish2);
	    
	    Dish dish3 = new Dish();
	    dish3.setDishName("gnocchi");
	    dish3.setCalories(700);
	    dish3.setQuality("high");
	    dish3.setCost(10);
	    dishes.add(dish3);
	    
	    Dish dish4 = new Dish();
	    dish4.setDishName("pizza");
	    dish4.setCalories(400);
	    dish4.setQuality("low");
	    dish4.setCost(7);
	    dishes.add(dish4);
	    
	    Dish dish5 = new Dish();
	    dish5.setDishName("snitzel");
	    dish5.setCalories(400);
	    dish5.setQuality("high");
	    dish5.setCost(12);
	    dishes.add(dish5);
	    
	    Beverage beverage1 = new Beverage();
	    beverage1.setBeverageName("coke");
	    beverage1.setVolume(35);
	    beverage1.setQuality("high");
	    beverage1.setCost(5);
	    beverages.add(beverage1);
		
	    Beverage beverage2 = new Beverage();
	    beverage2.setBeverageName("fanta");
	    beverage2.setVolume(35);
	    beverage2.setQuality("low");
	    beverage2.setCost(2);
	    beverages.add(beverage2);
	    
	    Beverage beverage3 = new Beverage();
	    beverage3.setBeverageName("wine");
	    beverage3.setVolume(50);
	    beverage3.setQuality("high");
	    beverage3.setCost(5);
	    beverages.add(beverage3);
	    
	    Beverage beverage4 = new Beverage();
	    beverage4.setBeverageName("beer");
	    beverage4.setVolume(50);
	    beverage4.setQuality("low");
	    beverage4.setCost(2);
	    beverages.add(beverage4);
	    
	    Beverage beverage5 = new Beverage();
	    beverage5.setBeverageName("sprite");
	    beverage5.setVolume(35);
	    beverage5.setQuality("high");
	    beverage5.setCost(5);
	    beverages.add(beverage5);
	    
	    restaurant.createMenu(dishes, beverages);
	    Mockito.verify(out).println("ERROR!!! All the high quality dishes must have the same price");
	}
	
	@Test
	public void testIncorrectLowDishesCost()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    List<Dish> dishes = new ArrayList<Dish>();
	    List<Beverage> beverages = new ArrayList<Beverage>();
	    
	    Dish dish1 = new Dish();
	    dish1.setDishName("grilled chicken");
	    dish1.setCalories(400);
	    dish1.setQuality("high");
	    dish1.setCost(12);
	    dishes.add(dish1);
	    
	    Dish dish2 = new Dish();
	    dish2.setDishName("lasagna");
	    dish2.setCalories(800);
	    dish2.setQuality("low");
	    dish2.setCost(7);
	    dishes.add(dish2);
	    
	    Dish dish3 = new Dish();
	    dish3.setDishName("gnocchi");
	    dish3.setCalories(700);
	    dish3.setQuality("high");
	    dish3.setCost(12);
	    dishes.add(dish3);
	    
	    Dish dish4 = new Dish();
	    dish4.setDishName("pizza");
	    dish4.setCalories(400);
	    dish4.setQuality("low");
	    dish4.setCost(6);
	    dishes.add(dish4);
	    
	    Dish dish5 = new Dish();
	    dish5.setDishName("snitzel");
	    dish5.setCalories(400);
	    dish5.setQuality("high");
	    dish5.setCost(12);
	    dishes.add(dish5);
	    
	    Beverage beverage1 = new Beverage();
	    beverage1.setBeverageName("coke");
	    beverage1.setVolume(35);
	    beverage1.setQuality("high");
	    beverage1.setCost(5);
	    beverages.add(beverage1);
		
	    Beverage beverage2 = new Beverage();
	    beverage2.setBeverageName("fanta");
	    beverage2.setVolume(35);
	    beverage2.setQuality("low");
	    beverage2.setCost(2);
	    beverages.add(beverage2);
	    
	    Beverage beverage3 = new Beverage();
	    beverage3.setBeverageName("wine");
	    beverage3.setVolume(50);
	    beverage3.setQuality("high");
	    beverage3.setCost(5);
	    beverages.add(beverage3);
	    
	    Beverage beverage4 = new Beverage();
	    beverage4.setBeverageName("beer");
	    beverage4.setVolume(50);
	    beverage4.setQuality("low");
	    beverage4.setCost(2);
	    beverages.add(beverage4);
	    
	    Beverage beverage5 = new Beverage();
	    beverage5.setBeverageName("sprite");
	    beverage5.setVolume(35);
	    beverage5.setQuality("high");
	    beverage5.setCost(5);
	    beverages.add(beverage5);
	    
	    restaurant.createMenu(dishes, beverages);
	    Mockito.verify(out).println("ERROR!!! all the low quality dishes must have the same price");
	}
	
	@Test
	public void testIncorrectHighBeveragesCost()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    List<Dish> dishes = new ArrayList<Dish>();
	    List<Beverage> beverages = new ArrayList<Beverage>();
	    
	    Dish dish1 = new Dish();
	    dish1.setDishName("grilled chicken");
	    dish1.setCalories(400);
	    dish1.setQuality("high");
	    dish1.setCost(12);
	    dishes.add(dish1);
	    
	    Dish dish2 = new Dish();
	    dish2.setDishName("lasagna");
	    dish2.setCalories(800);
	    dish2.setQuality("low");
	    dish2.setCost(7);
	    dishes.add(dish2);
	    
	    Dish dish3 = new Dish();
	    dish3.setDishName("gnocchi");
	    dish3.setCalories(700);
	    dish3.setQuality("high");
	    dish3.setCost(12);
	    dishes.add(dish3);
	    
	    Dish dish4 = new Dish();
	    dish4.setDishName("pizza");
	    dish4.setCalories(400);
	    dish4.setQuality("low");
	    dish4.setCost(7);
	    dishes.add(dish4);
	    
	    Dish dish5 = new Dish();
	    dish5.setDishName("snitzel");
	    dish5.setCalories(400);
	    dish5.setQuality("high");
	    dish5.setCost(12);
	    dishes.add(dish5);
	    
	    Beverage beverage1 = new Beverage();
	    beverage1.setBeverageName("coke");
	    beverage1.setVolume(35);
	    beverage1.setQuality("high");
	    beverage1.setCost(7);
	    beverages.add(beverage1);
		
	    Beverage beverage2 = new Beverage();
	    beverage2.setBeverageName("fanta");
	    beverage2.setVolume(35);
	    beverage2.setQuality("low");
	    beverage2.setCost(2);
	    beverages.add(beverage2);
	    
	    Beverage beverage3 = new Beverage();
	    beverage3.setBeverageName("wine");
	    beverage3.setVolume(50);
	    beverage3.setQuality("high");
	    beverage3.setCost(5);
	    beverages.add(beverage3);
	    
	    Beverage beverage4 = new Beverage();
	    beverage4.setBeverageName("beer");
	    beverage4.setVolume(50);
	    beverage4.setQuality("low");
	    beverage4.setCost(2);
	    beverages.add(beverage4);
	    
	    Beverage beverage5 = new Beverage();
	    beverage5.setBeverageName("sprite");
	    beverage5.setVolume(35);
	    beverage5.setQuality("high");
	    beverage5.setCost(5);
	    beverages.add(beverage5);
	    
	    restaurant.createMenu(dishes, beverages);
	    Mockito.verify(out).println("ERROR!!! all the high quality beverages must have the same price");
	}
	
	@Test
	public void testIncorrectLowBeveragesCost()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    List<Dish> dishes = new ArrayList<Dish>();
	    List<Beverage> beverages = new ArrayList<Beverage>();
	    
	    Dish dish1 = new Dish();
	    dish1.setDishName("grilled chicken");
	    dish1.setCalories(400);
	    dish1.setQuality("high");
	    dish1.setCost(12);
	    dishes.add(dish1);
	    
	    Dish dish2 = new Dish();
	    dish2.setDishName("lasagna");
	    dish2.setCalories(800);
	    dish2.setQuality("low");
	    dish2.setCost(7);
	    dishes.add(dish2);
	    
	    Dish dish3 = new Dish();
	    dish3.setDishName("gnocchi");
	    dish3.setCalories(700);
	    dish3.setQuality("high");
	    dish3.setCost(12);
	    dishes.add(dish3);
	    
	    Dish dish4 = new Dish();
	    dish4.setDishName("pizza");
	    dish4.setCalories(400);
	    dish4.setQuality("low");
	    dish4.setCost(7);
	    dishes.add(dish4);
	    
	    Dish dish5 = new Dish();
	    dish5.setDishName("snitzel");
	    dish5.setCalories(400);
	    dish5.setQuality("high");
	    dish5.setCost(12);
	    dishes.add(dish5);
	    
	    Beverage beverage1 = new Beverage();
	    beverage1.setBeverageName("coke");
	    beverage1.setVolume(35);
	    beverage1.setQuality("high");
	    beverage1.setCost(5);
	    beverages.add(beverage1);
		
	    Beverage beverage2 = new Beverage();
	    beverage2.setBeverageName("fanta");
	    beverage2.setVolume(35);
	    beverage2.setQuality("low");
	    beverage2.setCost(1);
	    beverages.add(beverage2);
	    
	    Beverage beverage3 = new Beverage();
	    beverage3.setBeverageName("wine");
	    beverage3.setVolume(50);
	    beverage3.setQuality("high");
	    beverage3.setCost(5);
	    beverages.add(beverage3);
	    
	    Beverage beverage4 = new Beverage();
	    beverage4.setBeverageName("beer");
	    beverage4.setVolume(50);
	    beverage4.setQuality("low");
	    beverage4.setCost(2);
	    beverages.add(beverage4);
	    
	    Beverage beverage5 = new Beverage();
	    beverage5.setBeverageName("sprite");
	    beverage5.setVolume(35);
	    beverage5.setQuality("high");
	    beverage5.setCost(5);
	    beverages.add(beverage5);
	    
	    restaurant.createMenu(dishes, beverages);
	    Mockito.verify(out).println("ERROR!!! all the low quality beverages must have the same price");
	}
	
	@Test
	public void updateBudgetBasedOnCostOfIngredients1()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    
	    restaurant.setBudget(6000);
	    restaurant.setCostHighQualityDishes(10);
		restaurant.setCostLowQualityDishes(3);
		restaurant.setCostHighQualityBeverages(3);
		restaurant.setCostLowQualityBeverages(1);
		
		restaurant.addSumForIngredients(4 * restaurant.getCostHighQualityDishes());
		restaurant.addSumForIngredients(6 * restaurant.getCostLowQualityDishes());
		restaurant.addSumForIngredients(6 * restaurant.getCostHighQualityBeverages());
		restaurant.addSumForIngredients(4 * restaurant.getLowHighQualityBeverages());
		
		restaurant.payIngredients();
		
		Assert.assertEquals(5920, restaurant.getBudget());
	}
	
	@Test
	public void updateBudgetBasedOnCostOfIngredients2()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    
	    restaurant.setBudget(60);
	    restaurant.setCostHighQualityDishes(10);
		restaurant.setCostLowQualityDishes(3);
		restaurant.setCostHighQualityBeverages(3);
		restaurant.setCostLowQualityBeverages(1);
		
		restaurant.addSumForIngredients(4 * restaurant.getCostHighQualityDishes());
		restaurant.addSumForIngredients(3 * restaurant.getCostLowQualityDishes());
		restaurant.addSumForIngredients(3 * restaurant.getCostHighQualityBeverages());
		restaurant.addSumForIngredients(2 * restaurant.getLowHighQualityBeverages());
		
		restaurant.payIngredients();
		
		Assert.assertEquals(0, restaurant.getBudget());
		Mockito.verify(out).println("GAME OVER");
	}
	
	@Test
	public void updateBudgetBasedOnCostOfIngredients3()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    
	    restaurant.setBudget(6000);
	    restaurant.setCostHighQualityDishes(10);
		restaurant.setCostLowQualityDishes(3);
		restaurant.setCostHighQualityBeverages(3);
		restaurant.setCostLowQualityBeverages(1);
		
		restaurant.addSumForIngredients(16 * restaurant.getCostHighQualityDishes());
		restaurant.addSumForIngredients(2 * restaurant.getCostLowQualityDishes());
		restaurant.addSumForIngredients(12 * restaurant.getCostHighQualityBeverages());
		restaurant.addSumForIngredients(6 * restaurant.getLowHighQualityBeverages());
		
		restaurant.payIngredients();
		
		Assert.assertEquals(5792, restaurant.getBudget());
	}
	
	@Test
	public void updateBudgetBasedOnSalary1()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    
	    restaurant.setBudget(8000);
	    
	    List<Waiter> waiters = new ArrayList<Waiter>();

	    Waiter waiter1 = new Waiter();
	    waiter1.setName("Naved");
	    waiter1.setSurname("Ahmed");
	    waiter1.setExpericenLevel(0);
	    waiter1.setSalary(200);
	    waiters.add(waiter1);
	    
	    Waiter waiter2 = new Waiter();
	    waiter2.setName("Fabrizio");
	    waiter2.setSurname("Maggi");
	    waiter2.setExpericenLevel(0);
	    waiter2.setSalary(200);
	    waiters.add(waiter2);
	    
	    Waiter waiter3 = new Waiter();
	    waiter3.setName("Amnir");
	    waiter3.setSurname("Hadachi");
	    waiter3.setExpericenLevel(0);
	    waiter3.setSalary(200);
	    waiters.add(waiter3);
	    
	    restaurant.setWaiters(waiters);
	    
	    List<Chef> chefs = new ArrayList<Chef>();
	    Chef chef = new Chef();
	    chef.setName("Luciano");
	    chef.setSurname("Garcia");
	    chef.setExpericenLevel(0);
	    chef.setSalary(300);
	    chef.setTaxCode("0099");
	    chefs.add(chef);
	    
	    restaurant.setChef(chefs);
	    
	    List<Barman> barmans = new ArrayList<Barman>();
	    Barman barman = new Barman();
	    barman.setName("Abel");
	    barman.setSurname("Armas");
	    barman.setExpericenLevel(0);
	    barman.setSalary(300);
	    barmans.add(barman);
	    
	    restaurant.setBarman(barmans);
		
	    restaurant.paySalaries();
		
		Assert.assertEquals(6800, restaurant.getBudget());
	}
	
	@Test
	public void updateBudgetBasedOnSalary2()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    
	    restaurant.setBudget(1000);
	    
	    List<Waiter> waiters = new ArrayList<Waiter>();

	    Waiter waiter1 = new Waiter();
	    waiter1.setName("Naved");
	    waiter1.setSurname("Ahmed");
	    waiter1.setExpericenLevel(0);
	    waiter1.setSalary(200);
	    waiters.add(waiter1);
	    
	    Waiter waiter2 = new Waiter();
	    waiter2.setName("Fabrizio");
	    waiter2.setSurname("Maggi");
	    waiter2.setExpericenLevel(0);
	    waiter2.setSalary(200);
	    waiters.add(waiter2);
	    
	    Waiter waiter3 = new Waiter();
	    waiter3.setName("Amnir");
	    waiter3.setSurname("Hadachi");
	    waiter3.setExpericenLevel(0);
	    waiter3.setSalary(200);
	    waiters.add(waiter3);
	    
	    restaurant.setWaiters(waiters);
	    
	    List<Chef> chefs = new ArrayList<Chef>();
	    Chef chef = new Chef();
	    chef.setName("Luciano");
	    chef.setSurname("Garcia");
	    chef.setExpericenLevel(0);
	    chef.setSalary(300);
	    chef.setTaxCode("0099");
	    chefs.add(chef);
	    
	    restaurant.setChef(chefs);
	    
	    List<Barman> barmans = new ArrayList<Barman>();
	    Barman barman = new Barman();
	    barman.setName("Abel");
	    barman.setSurname("Armas");
	    barman.setExpericenLevel(0);
	    barman.setSalary(300);
	    barmans.add(barman);
	    
	    restaurant.setBarman(barmans);
		
	    restaurant.paySalaries();
		
	    Mockito.verify(out).println("GAME OVER");
	}
	
	@Test
	public void updateBudgetBasedOnSalary3()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    
	    restaurant.setBudget(1800);
	    
	    List<Waiter> waiters = new ArrayList<Waiter>();

	    Waiter waiter1 = new Waiter();
	    waiter1.setName("Naved");
	    waiter1.setSurname("Ahmed");
	    waiter1.setExpericenLevel(0);
	    waiter1.setSalary(200);
	    waiters.add(waiter1);
	    
	    Waiter waiter2 = new Waiter();
	    waiter2.setName("Fabrizio");
	    waiter2.setSurname("Maggi");
	    waiter2.setExpericenLevel(1);
	    waiter2.setSalary(300);
	    waiters.add(waiter2);
	    
	    Waiter waiter3 = new Waiter();
	    waiter3.setName("Amnir");
	    waiter3.setSurname("Hadachi");
	    waiter3.setExpericenLevel(2);
	    waiter3.setSalary(400);
	    waiters.add(waiter3);
	    
	    restaurant.setWaiters(waiters);
	    
	    List<Chef> chefs = new ArrayList<Chef>();
	    Chef chef = new Chef();
	    chef.setName("Luciano");
	    chef.setSurname("Garcia");
	    chef.setExpericenLevel(1);
	    chef.setSalary(400);
	    chef.setTaxCode("0099");
	    chefs.add(chef);
	    
	    restaurant.setChef(chefs);
	    
	    List<Barman> barmans = new ArrayList<Barman>();
	    Barman barman = new Barman();
	    barman.setName("Abel");
	    barman.setSurname("Armas");
	    barman.setExpericenLevel(2);
	    barman.setSalary(500);
	    barmans.add(barman);
	    
	    restaurant.setBarman(barmans);
		
	    restaurant.paySalaries();
		
		Assert.assertEquals(0, restaurant.getBudget());
		Mockito.verify(out).println("GAME OVER");
	}
	
	@Test
	public void testTrainEmployee1(){
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);

		
		List<Waiter> waiters = new ArrayList<Waiter>();

	    Waiter waiter1 = new Waiter();
	    waiter1.setName("Naved");
	    waiter1.setExpericenLevel(0);
	    waiters.add(waiter1);
	    
	    Waiter waiter2 = new Waiter();
	    waiter2.setName("Fabrizio");
	    waiter2.setExpericenLevel(0);
	    waiters.add(waiter2);
	    
	    Waiter waiter3 = new Waiter();
	    waiter3.setName("Amnir");
	    waiter3.setExpericenLevel(0);
	    waiters.add(waiter3);
	    
	    restaurant.setWaiters(waiters);
	    
	    List<Chef> chefs = new ArrayList<Chef>();
	    Chef chef = new Chef();
	    chef.setName("Luciano");
	    chef.setExpericenLevel(0);
	    chefs.add(chef);
	    
	    restaurant.setChef(chefs);
	    
	    List<Barman> barmans = new ArrayList<Barman>();
	    Barman barman = new Barman();
	    barman.setName("Abel");
	    barman.setExpericenLevel(0);
	    barmans.add(barman);
	    
	    restaurant.setBarman(barmans);
	    
	    restaurant.setBudget(10000);
	    
	    restaurant.increaseExperience(1, "Luciano", "chef");
	    
		Mockito.verify(out).println("The chef experience level increased");
		Assert.assertEquals(8800, restaurant.getBudget());	
	}
	
	@Test
	public void testTrainEmployee2(){
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);

		
		List<Waiter> waiters = new ArrayList<Waiter>();

	    Waiter waiter1 = new Waiter();
	    waiter1.setName("Naved");
	    waiter1.setExpericenLevel(0);
	    waiters.add(waiter1);
	    
	    Waiter waiter2 = new Waiter();
	    waiter2.setName("Fabrizio");
	    waiter2.setExpericenLevel(0);
	    waiters.add(waiter2);
	    
	    Waiter waiter3 = new Waiter();
	    waiter3.setName("Amnir");
	    waiter3.setExpericenLevel(0);
	    waiters.add(waiter3);
	    
	    restaurant.setWaiters(waiters);
	    
	    List<Chef> chefs = new ArrayList<Chef>();
	    Chef chef = new Chef();
	    chef.setName("Luciano");
	    chef.setExpericenLevel(0);
	    chefs.add(chef);
	    
	    restaurant.setChef(chefs);
	    
	    List<Barman> barmans = new ArrayList<Barman>();
	    Barman barman = new Barman();
	    barman.setName("Abel");
	    barman.setExpericenLevel(0);
	    barmans.add(barman);
	    
	    restaurant.setBarman(barmans);
	    
	    restaurant.setBudget(10000);
	    
	    restaurant.increaseExperience(1, "Abel", "barman");
	    
		Mockito.verify(out).println("The barman experience level increased");
		Assert.assertEquals(8800, restaurant.getBudget());
	}
	
	@Test
	public void testTrainEmployee3(){
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);

		
		List<Waiter> waiters = new ArrayList<Waiter>();

	    Waiter waiter1 = new Waiter();
	    waiter1.setName("Naved");
	    waiter1.setExpericenLevel(0);
	    waiters.add(waiter1);
	    
	    Waiter waiter2 = new Waiter();
	    waiter2.setName("Fabrizio");
	    waiter2.setExpericenLevel(0);
	    waiters.add(waiter2);
	    
	    Waiter waiter3 = new Waiter();
	    waiter3.setName("Amnir");
	    waiter3.setExpericenLevel(0);
	    waiters.add(waiter3);
	    
	    restaurant.setWaiters(waiters);
	    
	    List<Chef> chefs = new ArrayList<Chef>();
	    Chef chef = new Chef();
	    chef.setName("Luciano");
	    chef.setExpericenLevel(0);
	    chefs.add(chef);
	    
	    restaurant.setChef(chefs);
	    
	    List<Barman> barmans = new ArrayList<Barman>();
	    Barman barman = new Barman();
	    barman.setName("Abel");
	    barman.setExpericenLevel(0);
	    barmans.add(barman);
	    
	    restaurant.setBarman(barmans);
	    
	    restaurant.setBudget(10000);
	    
	    restaurant.increaseExperience(1, "Naved", "waiter");
	    
		Mockito.verify(out).println("The waiter experience level increased");
		Assert.assertEquals(9200, restaurant.getBudget());
	}
	
	@Test
	public void testTrainEmployee4(){
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);

		
		List<Waiter> waiters = new ArrayList<Waiter>();

	    Waiter waiter1 = new Waiter();
	    waiter1.setName("Naved");
	    waiter1.setExpericenLevel(0);
	    waiters.add(waiter1);
	    
	    Waiter waiter2 = new Waiter();
	    waiter2.setName("Fabrizio");
	    waiter2.setExpericenLevel(0);
	    waiters.add(waiter2);
	    
	    Waiter waiter3 = new Waiter();
	    waiter3.setName("Amnir");
	    waiter3.setExpericenLevel(0);
	    waiters.add(waiter3);
	    
	    restaurant.setWaiters(waiters);
	    
	    List<Chef> chefs = new ArrayList<Chef>();
	    Chef chef = new Chef();
	    chef.setName("Luciano");
	    chef.setExpericenLevel(0);
	    chefs.add(chef);
	    
	    restaurant.setChef(chefs);
	    
	    List<Barman> barmans = new ArrayList<Barman>();
	    Barman barman = new Barman();
	    barman.setName("Abel");
	    barman.setExpericenLevel(0);
	    barmans.add(barman);
	    
	    restaurant.setBarman(barmans);
	    
	    restaurant.setBudget(700);
	    
	    restaurant.increaseExperience(1, "Luciano", "chef");
	    
		Mockito.verify(out).println("The chef experience level failed to increase");
	}
	
	@Test
	public void testTrainEmployee5(){
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);

		
		List<Waiter> waiters = new ArrayList<Waiter>();

	    Waiter waiter1 = new Waiter();
	    waiter1.setName("Naved");
	    waiter1.setExpericenLevel(0);
	    waiters.add(waiter1);
	    
	    Waiter waiter2 = new Waiter();
	    waiter2.setName("Fabrizio");
	    waiter2.setExpericenLevel(0);
	    waiters.add(waiter2);
	    
	    Waiter waiter3 = new Waiter();
	    waiter3.setName("Amnir");
	    waiter3.setExpericenLevel(0);
	    waiters.add(waiter3);
	    
	    restaurant.setWaiters(waiters);
	    
	    List<Chef> chefs = new ArrayList<Chef>();
	    Chef chef = new Chef();
	    chef.setName("Luciano");
	    chef.setExpericenLevel(0);
	    chefs.add(chef);
	    
	    restaurant.setChef(chefs);
	    
	    List<Barman> barmans = new ArrayList<Barman>();
	    Barman barman = new Barman();
	    barman.setName("Abel");
	    barman.setExpericenLevel(0);
	    barmans.add(barman);
	    
	    restaurant.setBarman(barmans);
	    
	    restaurant.setBudget(10000);
	    
	    restaurant.increaseExperience(2, "Fabrizio,Amnir", "waiter,waiter");
	    
		Mockito.verify(out).println("The two waiters experience level increased");
		Assert.assertEquals(8400, restaurant.getBudget());
	}
	
	@Test
	public void testTrainEmployee6(){
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);

		
		List<Waiter> waiters = new ArrayList<Waiter>();

	    Waiter waiter1 = new Waiter();
	    waiter1.setName("Naved");
	    waiter1.setExpericenLevel(0);
	    waiters.add(waiter1);
	    
	    Waiter waiter2 = new Waiter();
	    waiter2.setName("Fabrizio");
	    waiter2.setExpericenLevel(0);
	    waiters.add(waiter2);
	    
	    Waiter waiter3 = new Waiter();
	    waiter3.setName("Amnir");
	    waiter3.setExpericenLevel(0);
	    waiters.add(waiter3);
	    
	    restaurant.setWaiters(waiters);
	    
	    List<Chef> chefs = new ArrayList<Chef>();
	    Chef chef = new Chef();
	    chef.setName("Luciano");
	    chef.setExpericenLevel(0);
	    chefs.add(chef);
	    
	    restaurant.setChef(chefs);
	    
	    List<Barman> barmans = new ArrayList<Barman>();
	    Barman barman = new Barman();
	    barman.setName("Abel");
	    barman.setExpericenLevel(0);
	    barmans.add(barman);
	    
	    restaurant.setBarman(barmans);
	    
	    restaurant.setBudget(880);
	    
	    restaurant.increaseExperience(2, "Naved,Abel", "waiter,barman");
	    
		Mockito.verify(out).println("The waiter experience level increased and the barman experience level failed to increase");
		Assert.assertEquals(80, restaurant.getBudget());
	}
	
	@Test
	public void testAssignWaiters1()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    
	    List<Table> tables = new ArrayList<Table>();
	    
	    Table table1 = new Table();
	    table1.setNumber(1);
	    tables.add(table1);
	    
	    Table table2 = new Table();
	    table2.setNumber(2);
	    tables.add(table2);
	    
	    Table table3 = new Table();
	    table3.setNumber(3);
	    tables.add(table3);
	    
	    Table table4 = new Table();
	    table4.setNumber(4);
	    tables.add(table4);
	    
	    Table table5 = new Table();
	    table5.setNumber(5);
	    tables.add(table5);
	    
	    Table table6 = new Table();
	    table6.setNumber(6);
	    tables.add(table6);
	    
	    Table table7 = new Table();
	    table7.setNumber(7);
	    tables.add(table7);
	    
	    Table table8 = new Table();
	    table8.setNumber(8);
	    tables.add(table8);
	    
	    Table table9 = new Table();
	    table9.setNumber(9);
	    tables.add(table9);
	    
	    restaurant.setTables(tables);
	    
	    
	    restaurant.getTable(1).setClients(2);
	    restaurant.getTable(2).setClients(2);
	    restaurant.getTable(4).setClients(2);
	    restaurant.getTable(6).setClients(2);
	    restaurant.getTable(9).setClients(2);
	    
	    List<Waiter> waitersList = new ArrayList<Waiter>();
	    
	    Waiter waiter1 = new Waiter();
    	waiter1.setName("Naved");
    	waiter1.setExpericenLevel(0);
    	waiter1.setSalary(200);
    	waitersList.add(waiter1);
    	
    	Waiter waiter2 = new Waiter();
    	waiter2.setName("Fabrizio");
    	waiter2.setExpericenLevel(0);
    	waiter2.setSalary(200);
    	waitersList.add(waiter2);
    	
    	Waiter waiter3 = new Waiter();
    	waiter3.setName("Amnir");
    	waiter3.setExpericenLevel(0);
    	waiter3.setSalary(200);
    	waitersList.add(waiter3);
    	
    	restaurant.setWaiters(waitersList);
    	
    	
    	List<Integer> numbers = new ArrayList<Integer>();
		List<String> waiters = new ArrayList<String>();
		
		numbers.add(2);
		numbers.add(9);
		numbers.add(4);
		numbers.add(6);
		numbers.add(1);
		
		waiters.add("Naved");
		waiters.add("Naved");
		waiters.add("Amnir");
		waiters.add("Fabrizio");
		waiters.add("Fabrizio");
		
		restaurant.assignTables(numbers, waiters);
		
		Mockito.verify(out).println("Successful Assignment");
	}
	
	@Test
	public void testAssignWaiters2()
	{
		RestaurantGame game = new RestaurantGame();
		Restaurant restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	    
	    List<Table> tables = new ArrayList<Table>();
	    
	    Table table1 = new Table();
	    table1.setNumber(1);
	    tables.add(table1);
	    
	    Table table2 = new Table();
	    table2.setNumber(2);
	    tables.add(table2);
	    
	    Table table3 = new Table();
	    table3.setNumber(3);
	    tables.add(table3);
	    
	    Table table4 = new Table();
	    table4.setNumber(4);
	    tables.add(table4);
	    
	    Table table5 = new Table();
	    table5.setNumber(5);
	    tables.add(table5);
	    
	    Table table6 = new Table();
	    table6.setNumber(6);
	    tables.add(table6);
	    
	    Table table7 = new Table();
	    table7.setNumber(7);
	    tables.add(table7);
	    
	    Table table8 = new Table();
	    table8.setNumber(8);
	    tables.add(table8);
	    
	    Table table9 = new Table();
	    table9.setNumber(9);
	    tables.add(table9);
	    
	    restaurant.setTables(tables);
	    
	    
	    restaurant.getTable(1).setClients(2);
	    restaurant.getTable(2).setClients(2);
	    restaurant.getTable(3).setClients(2);
	    restaurant.getTable(4).setClients(2);
	    restaurant.getTable(5).setClients(2);
	    
	    List<Waiter> waitersList = new ArrayList<Waiter>();
	    
	    Waiter waiter1 = new Waiter();
    	waiter1.setName("Naved");
    	waiter1.setExpericenLevel(0);
    	waiter1.setSalary(200);
    	waitersList.add(waiter1);
    	
    	Waiter waiter2 = new Waiter();
    	waiter2.setName("Fabrizio");
    	waiter2.setExpericenLevel(0);
    	waiter2.setSalary(200);
    	waitersList.add(waiter2);
    	
    	Waiter waiter3 = new Waiter();
    	waiter3.setName("Amnir");
    	waiter3.setExpericenLevel(0);
    	waiter3.setSalary(200);
    	waitersList.add(waiter3);
    	
    	restaurant.setWaiters(waitersList);
    	
    	
    	List<Integer> numbers = new ArrayList<Integer>();
		List<String> waiters = new ArrayList<String>();
		
		numbers.add(1);
		numbers.add(2);
		numbers.add(3);
		numbers.add(5);
		numbers.add(4);
		
		waiters.add("Naved");
		waiters.add("Naved");
		waiters.add("Naved");
		waiters.add("Fabrizio");
		waiters.add("Naved");
		
		restaurant.assignTables(numbers, waiters);
		
		Mockito.verify(out).println("ERROR!!! Naved has more than three tables assigned");
	}
}