package restaurant;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.mockito.Mockito;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RestaurantSteps {
	
	PrintStream out = null;
	RestaurantGame game;
	Restaurant restaurant = null;
	Player player = null;
	List<Dish> dishes = null;
	List<Beverage> beverages = null;
	
	@Before
	public void setUp(){
		out = Mockito.mock(PrintStream.class);
	    System.setOut(out); 
	}
	
	//////////////////////////////////////Feature 1 Steps//////////////////////////////////////
	@Given("^The \"([^\"]*)\" is created with the name \"([^\"]*)\"$")
	public void The_is_created_with_the_name(String entity, String name) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    //throw new PendingException();
		if(entity.equals("restaurant"))
		{
			restaurant = new Restaurant(name);
		}
		else if(entity.equals("player"))
		{
			player = new Player(name);
		}
	}

	@Given("^The restaurant budget is initialised to (\\d+)$")
	public void The_restaurant_budget_is_initialised_to(int budget) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    restaurant.setBudget(budget);
	}

	@When("^I start playing restaurant game$")
	public void I_start_playing_restaurant_game() throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    game = new RestaurantGame();
	    game.setRestaurant(restaurant);
	    game.setPlayer(player);
	    game.startGame();
	}

	@Then("^I should see \"([^\"]*)\"$")
	public void I_should_see(String message) throws Throwable {
	    // Express the Regexp above with the code you wish you had
		Mockito.verify(out).println(message);
	}
	
    //////////////////////////////////////Feature 2 Steps//////////////////////////////////////
	@Given("^The game has started$")
	public void The_game_has_started() throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    game = new RestaurantGame();
	}

	@Given("^The restaurant is created$")
	public void The_restaurant_is_created() throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    restaurant = new Restaurant();
	    game.setRestaurant(restaurant);
	}

	@Given("^the following set of \"([^\"]*)\"$")
	public void the_following_set_of(String entity, DataTable items) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    // For automatic conversion, change DataTable to List<YourType>
	    //dishes = new ArrayList<Dish>();
		if(entity.equals("Dishes")){
			dishes = items.asList(Dish.class);
		}
		else if(entity.equals("Beverages")){
			beverages = items.asList(Beverage.class);
		}		
	}

	@When("^I create a Menu$")
	public void I_create_a_Menu() throws Throwable {
	    // Express the Regexp above with the code you wish you had   
		restaurant.createMenu(dishes, beverages);
	}

	@Then("^I should see the \"([^\"]*)\"$")
	public void I_should_see_the(String message) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    Mockito.verify(out).println(message);
	}
	
	
	////////////////////////////////////Feature 4 Steps//////////////////////////////////////
	@Given("^The budget is (\\d+)$")
	public void The_budget_is(int budget) throws Throwable {
	    restaurant.setBudget(budget);
	}

	@Given("^cost of ingredients for \"([^\"]*)\" is (\\d+)$")
	public void cost_of_ingredients_for_is(String typeOfItem, int cost) throws Throwable {
		if(typeOfItem.equals("high quality dishes"))
		{
			restaurant.setCostHighQualityDishes(cost);
		}
		else if(typeOfItem.equals("low quality dishes"))
		{
			restaurant.setCostLowQualityDishes(cost);
		}
		else if(typeOfItem.equals("high quality beverages"))
		{
			restaurant.setCostHighQualityBeverages(cost);
		}
		else if(typeOfItem.equals("low quality beverages"))
		{
			restaurant.setCostLowQualityBeverages(cost);
		}
	}

	@Given("^(\\d+) clients choose \"([^\"]*)\"$")
	public void clients_choose(int clients, String typeOfItem) throws Throwable {

		if(typeOfItem.equals("high quality dishes"))
		{
			restaurant.addSumForIngredients(clients * restaurant.getCostHighQualityDishes());
		}
		else if(typeOfItem.equals("low quality dishes"))
		{
			restaurant.addSumForIngredients(clients * restaurant.getCostLowQualityDishes());
		}
		else if(typeOfItem.equals("high quality beverages"))
		{
			restaurant.addSumForIngredients(clients * restaurant.getCostHighQualityBeverages());
		}
		else if(typeOfItem.equals("low quality beverages"))
		{
			restaurant.addSumForIngredients(clients * restaurant.getLowHighQualityBeverages());
		}
	}

	@When("^The budget is updated based on the \"([^\"]*)\"$")
	public void The_budget_is_updated_based_on_the(String expenseType) throws Throwable {
		if(expenseType.equals("expenses"))
		{
			this.restaurant.payIngredients();
		}
		else if(expenseType.equals("salary"))
		{
			this.restaurant.paySalaries();
		}
	}

	@Then("^The budget should be (\\d+)$")
	public void The_budget_should_be(int budget) throws Throwable {

		Assert.assertEquals(budget, restaurant.getBudget());
	}

	@Then("^Game Over$")
	public void Game_Over() throws Throwable {
	    Mockito.verify(out).println("GAME OVER");
	}

	@Given("^The restaurant has the following employee\\(s\\) as \"([^\"]*)\"$")
	public void The_restaurant_has_the_following_employee_s_as(String employeeType, DataTable employees) throws Throwable {

		if(employeeType.equals("waiters"))
		{
			List<Waiter> waiters = employees.asList(Waiter.class); 
			restaurant.setWaiters(waiters);
		}
		else if(employeeType.equals("chef"))
		{
			List<Chef> chefs = employees.asList(Chef.class);
			restaurant.setChef(chefs);
		}
		else if(employeeType.equals("barman"))
		{
			List<Barman> barmans = employees.asList(Barman.class);
			restaurant.setBarman(barmans);
		}
	}

    ////////////////////////////////////Feature 5 Steps//////////////////////////////////////
	@Given("^The list of employees$")
	public void The_list_of_employees(DataTable employees) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    // For automatic conversion, change DataTable to List<YourType>
	    List<Employee> employeeList = employees.asList(Employee.class);
	    
	    List<Chef> chefs = new ArrayList<Chef>();
	    List<Waiter> waiters = new ArrayList<Waiter>();
	    List<Barman> barmans = new ArrayList<Barman>();
	    
	    for(Employee employee : employeeList){
	    	if(employee.getJob().equals("chef")){
	    		Chef chef1 = new Chef();
	    		chef1.setName(employee.getName());
	    		chef1.setExpericenLevel(employee.getExperienceLevel());
	    		chefs.add(chef1);
	    	}
	    	else if (employee.getJob().equals("waiter")){
	    		Waiter waiter1 = new Waiter();
	    		waiter1.setName(employee.getName());
	    		waiter1.setExpericenLevel(employee.getExperienceLevel());
	    		waiters.add(waiter1);
	    	}
	    	else if(employee.getJob().equals("barman")){
	    		Barman barman1 = new Barman();
	    		barman1.setName(employee.getName());
	    		barman1.setExpericenLevel(employee.getExperienceLevel());
	    		barmans.add(barman1);
	    	}
	    }
	    
	    restaurant.setBarman(barmans);
	    restaurant.setWaiters(waiters);
	    restaurant.setChef(chefs);
	}

	@When("^I want to increase the experience of (\\d+) employee\\(s\\) \"([^\"]*)\" with the job \"([^\"]*)\"$")
	public void I_want_to_increase_the_experience_of_employee_s_with_the_job(int employeesNumber, String employeesName, String employeesJob) throws Throwable {
	    restaurant.increaseExperience(employeesNumber, employeesName, employeesJob);
	}

	@Then("^The budget should update to (\\d+)$")
	public void The_budget_should_update_to(int budget) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    Assert.assertEquals(budget, restaurant.getBudget());
	}
	
	
    ////////////////////////////////////Feature 6 Steps//////////////////////////////////////

	@Given("^The restaurant has following tables$")
	public void The_restaurant_has_following_tables(DataTable tables) throws Throwable {
		List<Table> tableList;
		tableList = tables.asList(Table.class);
	    restaurant.setTables(tableList);
	}

	@Given("^the following clients are randomly assigned to the tables$")
	public void the_following_clients_are_randomly_assigned_to_the_tables(DataTable assignedTables) throws Throwable {
	    List<Table> assigned = assignedTables.asList(Table.class);
		for(Table table : assigned)
		{
			restaurant.getTable(table.getNumber()).setClients(table.getClients());
		}
	}

	@Given("^The restaurant has (\\d+) waiters \"([^\"]*)\" all with experience level (\\d+) and salary (\\d+)$")
	public void The_restaurant_has_waiters_all_with_experience_level_and_salary(int numberOfWaiters, String waitersNames, int experience, int salary) throws Throwable {
	    String[] waiters = waitersNames.split(", ");
	    
	    List<Waiter> waitersList = new ArrayList<Waiter>();
	    
	    for(int i=0; i<waiters.length; i++)
	    {
	    	Waiter waiter = new Waiter();
	    	waiter.setName(waiters[i]);
	    	waiter.setExpericenLevel(experience);
	    	waiter.setSalary(salary);
	    	waitersList.add(waiter);
	    }
	    
	    restaurant.setWaiters(waitersList);
	}

	@When("^I assigned the following waiters to the table$")
	public void I_assigned_the_following_waiters_to_the_table(DataTable tablesWithWaiters) throws Throwable {
		List<Table> assigned = tablesWithWaiters.asList(Table.class);
		
		List<Integer> numbers = new ArrayList<Integer>();
		List<String> waiters = new ArrayList<String>();
		
		for(Table table : assigned)
		{
			numbers.add(table.getNumber());
			waiters.add(table.getWaiter());
		}
		
		restaurant.assignTables(numbers, waiters);
	}

	@Then("^I should see the message \"([^\"]*)\"$")
	public void I_should_see_the_message(String message) throws Throwable {
		Mockito.verify(out).println(message);
	}
}
