package restaurant;

public class Dish {
	
	private String dishName;
	private int calories;
	private String quality;
	private int cost;
	
	public String getQuality() {
		return this.quality;
	}

	public int getCost() {
		return this.cost;
	}

	public void setDishName(String dishName) {
		this.dishName = dishName;
	}

	public void setCalories(int calories) {
		this.calories = calories;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
}
