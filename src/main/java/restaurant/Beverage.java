package restaurant;

public class Beverage {
	private String beverageName;
	private int volume;
	private String quality;
	private int cost;
	
	public String getQuality() {
		return quality;
	}

	public int getCost() {
		return this.cost;
	}

	public void setBeverageName(String beverageName) {
		this.beverageName = beverageName;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
}
