package restaurant;

public class Barman {

	private String name;
	private String surname;
	private int experienceLevel;
	private int salary;
	
	public int getSalary() {
		return this.salary;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public void setExpericenLevel(int experienceLevel)
	{
		this.experienceLevel = experienceLevel;
	}
	
	public void setSalary(int salary)
	{
		this.salary = salary;
	}

	public String getName() {
		return this.name;
	}

	public void increaseExperience() {
		this.experienceLevel++;
	}
}
