package restaurant;

import java.util.HashMap;
import java.util.List;

public class Restaurant {

	private String name;
	private int budget;
	private List<Dish> dishes;
	private List<Beverage> beverages;
	private int highQualityDishesPrice;
	private int lowQualityDishesPrice;
	private int highQualityBeveragesPrice;
	private int lowQualityBeveragesPrice;
	private List<Waiter> waiters;
	private List<Chef> chef;
	private List<Barman> barman;
	private List<Table> tables;
	
	private int sumForIngredients;
	
	public Restaurant(String name) {
		this.name = name;
		this.sumForIngredients = 0;
	}

	public Restaurant() {
		
	}

	public void setBudget(int budget) {
		this.budget = budget;
	}

	public String getName() {
		return this.name;
	}

	public int getBudget() {
		return this.budget;
	}

	public void createMenu(List<Dish> dishes, List<Beverage> beverages) {
		
		int highPrice = -1;
		int lowPrice = -1;
		
		for(Dish dish : dishes){
			if(dish.getQuality().equals("high")){
				if(highPrice == -1){
					highPrice = dish.getCost();
				}
				else{
					if(dish.getCost()!= highPrice){
						System.out.println("ERROR!!! All the high quality dishes must have the same price");
						return;
					}
				}
			}
			else if(dish.getQuality().equals("low")){
				if(lowPrice == -1){
					lowPrice = dish.getCost();
				}
				else{
					if(dish.getCost()!= lowPrice){
						System.out.println("ERROR!!! all the low quality dishes must have the same price");
						return;
					}
				}
			}
		}
		
		highPrice = -1;
		lowPrice = -1;
		
		for(Beverage beverage : beverages){
			if(beverage.getQuality().equals("high")){
				if(highPrice == -1){
					highPrice = beverage.getCost();
				}
				else{
					if(beverage.getCost()!= highPrice){
						System.out.println("ERROR!!! all the high quality beverages must have the same price");
						return;
					}
				}
			}
			else if(beverage.getQuality().equals("low")){
				if(lowPrice == -1){
					lowPrice = beverage.getCost();
				}
				else{
					if(beverage.getCost()!= lowPrice){
						System.out.println("ERROR!!! all the low quality beverages must have the same price");
						return;
					}
				}
			}
		}
		
		this.dishes = dishes;
		this.beverages = beverages;
		System.out.println("Restaurant menu is created");
	}

	public void setCostHighQualityDishes(int cost) {
		this.highQualityDishesPrice = cost;
	}

	public void setCostLowQualityDishes(int cost) {
		this.lowQualityDishesPrice = cost;
	}

	public void setCostHighQualityBeverages(int cost) {
		this.highQualityBeveragesPrice = cost;
	}

	public void setCostLowQualityBeverages(int cost) {
		this.lowQualityBeveragesPrice = cost;
	}

	public int getCostHighQualityDishes() {
		return this.highQualityDishesPrice;
	}

	public void addSumForIngredients(int cost) {
		this.sumForIngredients += cost;
	}

	public int getCostLowQualityDishes() {
		return this.lowQualityDishesPrice;
	}

	public int getCostHighQualityBeverages() {
		return this.highQualityBeveragesPrice;
	}

	public int getLowHighQualityBeverages() {
		return this.lowQualityBeveragesPrice;
	}

	public void payIngredients() {
		this.budget -= this.sumForIngredients;
		this.sumForIngredients = 0;
		
		if(this.budget <= 0)
		{
			System.out.println("GAME OVER");
		}
	}

	public void setWaiters(List<Waiter> waiters) {
		this.waiters = waiters;
	}

	public void setChef(List<Chef> chefs) {
		this.chef = chefs;
	}

	public void setBarman(List<Barman> barmans) {
		this.barman = barmans;
	}

	public void paySalaries() {
		int salaries = 0;
		
		for(Waiter waiter : this.waiters)
		{
			salaries += waiter.getSalary();
		}
		
		for(Chef chef : this.chef)
		{
			salaries += chef.getSalary();
		}
		
		for(Barman barman : this.barman)
		{
			salaries += barman.getSalary();
		}
		
		this.budget -= salaries;
		
		if(this.budget <= 0)
		{
			System.out.println("GAME OVER");
		}
	}

	public void increaseExperience(int employeesNumber, String employeesName, String employeesJob) {
		
		String[] names = employeesName.split(",");
		String[] jobs = employeesJob.split(",");
		int waitersIncreased = 0;
		String message = "";
		
		for(int i=0; i<names.length; i++)
		{
		
			if(jobs[i].equals("barman")){
				Barman barman1 = null;
				for(Barman barman : this.barman){
					if(barman.getName().equals(names[i])){
						barman1 = barman;
					}
				}
				if(barman1 != null){
					if(this.budget >= 1200){
						this.budget -= 1200;
						barman1.increaseExperience();
						if(i > 0){
							message += " and the barman experience level increased";
						}
						else{
							message += "The barman experience level increased";
						}
					}
					else{
						if(i > 0){
							message += " and the barman experience level failed to increase";
						}
						else{
							message += "The barman experience level failed to increase";
						}
					}
				}
			}
			else if(jobs[i].equals("chef")){
				Chef chef1 = null;
				for(Chef chef : this.chef){
					if(chef.getName().equals(names[i])){
						chef1 = chef;
					}
				}
				if(chef1 != null){
					if(this.budget >= 1200){
						this.budget -= 1200;
						chef1.increaseExperience();
						if(i > 0){
							message += " and the chef experience level increased";
						}
						else{
							message += "The chef experience level increased";
						}
					}
					else{
						if(i > 0){
							message += " and the chef experience level failed to increase";
						}
						else{
							message += "The chef experience level failed to increase";
						}
					}
				}
			}
			else if(jobs[i].equals("waiter")){
				Waiter waiter1 = null;
				for(Waiter waiter : this.waiters){
					if(waiter.getName().equals(names[i])){
						waiter1 = waiter;
					}	
				}
				if(waiter1 != null){
					if(this.budget >= 800){
						this.budget -= 800;
						waiter1.increaseExperience();
						if(i > 0){
							message += " and the waiter experience level increased";
						}
						else{
							message += "The waiter experience level increased";
						}
						waitersIncreased++;
					}
					else{
						if(i > 0){
							message += " and the waiter experience level failed to increase";
						}
						else{
							message += "The waiter experience level failed to increase";
						}
					}
				}
			}
		}
		
		if(!message.isEmpty()){
			System.out.println(message);
		}
		switch(waitersIncreased)
		{
			case 0:
				break;
			case 1:
				//System.out.println("The waiter experience level increased");
				break;
			case 2:
				System.out.println("The two waiters experience level increased");
				break;
			case 3:
				System.out.println("The three waiters experience level increased");
				break;
		}
	}

	public void setTables(List<Table> tableList) {
		this.tables = tableList;
	}

	public Table getTable(int number) {
		for(Table table : tables)
		{
			if(table.getNumber() == number)
			{
				return table;
			}
		}
		
		return null;
	}

	public void assignTables(List<Integer> numbers, List<String> waiters) {
		
		HashMap<String, Integer> tablesAssignedToEach = new HashMap<String, Integer>();
		
		
		
		for(int i=0; i<numbers.size(); i++)
		{
			int number = numbers.get(i);
			for(Table table : tables)
			{
				if(table.getNumber() == number)
				{
					table.setWaiter(waiters.get(i));
					
					int noOfTables = tablesAssignedToEach.getOrDefault(waiters.get(i), -1);
					
					if(!tablesAssignedToEach.containsKey(waiters.get(i)))
					{
						tablesAssignedToEach.put(waiters.get(i), 1);
					}
					else if(noOfTables <= 2)
					{
						tablesAssignedToEach.replace(waiters.get(i), noOfTables+1);
					}
					else
					{
						System.out.println("ERROR!!! " + waiters.get(i) + " has more than three tables assigned");
						return;
					}
				}
			}
		}
		
		System.out.println("Successful Assignment");
	}
}
