package restaurant;

public class Table {

	private int tableNumber;
	private int number_of_clients;
	private String assigned_waiter;
	
	public int getClients() {
		return number_of_clients;
	}

	public int getNumber() {
		return tableNumber;
	}

	public void setClients(int clients) {
		this.number_of_clients = clients;
	}

	public String getWaiter() {
		return assigned_waiter;
	}

	public void setWaiter(String waiter) {
		this.assigned_waiter = waiter;
	}

	public void setNumber(int number) {
		this.tableNumber = number;
	}
}
