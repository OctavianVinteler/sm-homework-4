package restaurant;

public class Employee {
	private String name;
	private String job;
	private String experience;
	
	public String getJob() {
		return this.job;
	}

	public String getName() {
		return this.name;
	}

	public int getExperienceLevel() {
		if(experience.equals("low")){
			return 0;
		}
		else if(experience.equals("medium")){
			return 1;
		}
		else if(experience.equals("high")){
			return 2;
		}
		return 0;
	}
}
