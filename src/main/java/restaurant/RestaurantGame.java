package restaurant;

public class RestaurantGame {

	Restaurant restaurant;
	Player player;
	
	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void startGame() {
		System.out.println("Welcome to " + restaurant.getName() + " Game!");
		System.out.println("Thank you, " + player.getName() + ".");
		System.out.println("The starting budget equals to " + restaurant.getBudget());
	}

}
